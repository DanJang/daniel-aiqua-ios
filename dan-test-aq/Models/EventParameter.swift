//
//  EventParameter.swift
//  dan-test-aq
//
//  Created by KR-Demo on 2021/10/31.
//

import Foundation
import Appier

struct EventParameter {
    var eventName: String = "product_viewed"
    var product_id: Int? = 1111
    var price: Double? = 20.5


    func logging() {
        //https://docs.aiqua.appier.com/docs/logging-events-with-parameters-for-ios-sdk
//        var event : [String:String] = [:]
//        event["num_products"] = "2"
//        event["my_param"] = "some_value"
//        event["some_other_param"] = "123"
//        event["another_param"] = "1234.23"
//        QGSdk.getSharedInstance().logEvent("my_custom_event", withParameters: event)
//        QGSdk.getSharedInstance().logEvent("registration_completed", withParameters: nil)
//        QGSdk.getSharedInstance().logEvent("category_viewed", withParameters: ["category": "apparels"])
        
        var paramObject: [String: Any] = [:]
        if eventName != "" {
            if product_id != 0 {
                paramObject["product_id"] = product_id
            }
            if price != 0.0 {
                paramObject["price"] = price
            }
            // fire
            QGSdk.getSharedInstance().logEvent(eventName, withParameters: paramObject)
            // call aiqua event logging
            let jsonData = try! JSONSerialization.data(withJSONObject: paramObject, options: [])
            let jsonString = String(data: jsonData, encoding: String.Encoding.utf8)!
            print(jsonString)
            
        }
    }
}
