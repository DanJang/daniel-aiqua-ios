//
//  UserProfile.swift
//  dan-test-aq
//
//  Created by KR-Demo on 2021/10/31.
//

import Foundation
import Appier

struct UserProfile {
    var user_id: String = "user_id_1111"
    var email: String = "ios@appier.com"
////    let dateFormatter = DateFormatter()
////    dateFormatter.dateFormat = "YYYY-MM-dd"
////    dateFormatter.timeZone = TimeZone(identifier: "UTC")
//    var birthdayDate: Date = dateFormatter.date(from:"2000-01-01")
    var birthdayDate: Date = Date(timeIntervalSince1970: 0)
    
    func logging () {
        //call aiqua profile logging
        if user_id != "", email != "" {
            
            // validate email
            if !isValidEmailAddress(emailAddressString: email) {
                return
            }
            
            //convert birthdayDate to String variable birthday, with format YYYY-MM-DD
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "YYYY-MM-dd"
            let birthdayStr = dateFormatter.string(from: birthdayDate)
            print(birthdayStr)
            
            //do profile logging
//            let jsonObject: [String: Any] = ["user_id": user_id, "email": email, "birthday": birthdayStr]
//            let jsonData = try! JSONSerialization.data(withJSONObject: jsonObject, options: [])
//            let jsonString = String(data: jsonData, encoding: String.Encoding.utf8)!
//            print(jsonString)
            // fire
            QGSdk.getSharedInstance().setName(user_id)
            let calendar = Calendar.current
            if let year = calendar.dateComponents([.year], from: birthdayDate).year {
                QGSdk.getSharedInstance().setYearOfBirth(NSNumber(value: year))
            }
            if let month = calendar.dateComponents([.month], from: birthdayDate).month{
                QGSdk.getSharedInstance().setMonthOfBirth(NSNumber(value: month))
            }
            if let day = calendar.dateComponents([.day], from: birthdayDate).day {
                QGSdk.getSharedInstance().setDayOfBirth(NSNumber(value: day))
            }
            QGSdk.getSharedInstance().setEmail(email)
            // we can send custom keys  https://docs.aiqua.appier.com/docs/logging-user-profile-information-for-ios-sdk
            QGSdk.getSharedInstance().setCustomKey("rating", withValue: "5")
            QGSdk.getSharedInstance().setCustomKey("vip", withValue: "Y")
            //  set the value empty?
            //QGSdk.getSharedInstance().setCustomKey("rating", withValue: NSNull.init())
            //QGSdk.getSharedInstance().setCustomKey("vip", withValue: "")
            
            
            // if it is 'release' version, Appier SDK send the aboves to the server every 15 seconds
            // in case of 'debug' version, every 1 sec
            // use below if you want to flush end of current function
            QGSdk.getSharedInstance().flush()
        }
        
        // REC
        getREC()
    }
    func isValidEmailAddress(emailAddressString: String) -> Bool {
        var returnValue = true
        let emailRegEx = "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"

        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            let nsString = emailAddressString as NSString
            let results = regex.matches(in: emailAddressString, range: NSRange(location: 0, length: nsString.length))
            if results.count == 0
            {
                returnValue = false
            }
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }

        return  returnValue
    }
    
    
    func getREC() {
        // get REC list https://docs.aiqua.appier.com/docs/ios-sdk-recommendation-20
        let parameters:[String:Any] = [
            "user_id": "1111", // Required if filtering purchased product based on user_id
//            "filter": [
//                "rules": [
//                    [
//                        "key": "category",
//                        "op": "eq",
//                        "val": "Women > Top",
//                    ],
//                ]
//            ],
            "num": 10
        ]
        let scenarioId:String = "85T45wUWVHXAnqR9jpXZqt"
        let productId:String = "1111" // nil
        print("scenarioId: " + scenarioId)
        QGSdk.getSharedInstance().getRecommendationWithScenarioId(scenarioId, withProductId:productId, withQueryParameters: parameters, withCompletionHandler: { response in
            if response != nil {
                // Process response here
                print(response)
            }
        })
    }
    
    
}
