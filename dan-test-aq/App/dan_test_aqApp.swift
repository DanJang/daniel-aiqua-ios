//
//  dan_test_aqApp.swift
//  dan-test-aq
//
//  Created by KR-Demo on 2021/10/15.
//

import SwiftUI
import Appier
import UserNotifications
import AppTrackingTransparency

// after creating below whole codes
// you should add     @UIApplicationDelegateAdaptor(AppDelegate.self) var appDelegate  to @main
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        
        // Override point for customization after application launch
        let QG = QGSdk.getSharedInstance()
        #if DEBUG
        QG.onStart("d023d23bac0cf1e8e545", withAppGroup:"group.appier.dan-test-aq.notification", setDevProfile: true)
        #else
        QG.onStart("d023d23bac0cf1e8e545", withAppGroup:"group.appier.dan-test-aq.notification", setDevProfile: false)
        // AQ SDK flush the data to AQ server every 15 seconds to send data by default, but you can change the interval here
        //  The interval is 1 second if it is DEBUG mode
        //  https://docs.aiqua.appier.com/docs/configure-batching-for-the-ios-sdk
        QGSdk.getSharedInstance().flushInterval = 15
        #endif
        // we can store the notification : https://docs.aiqua.appier.com/docs/storing-push-notification-for-ios
        QGSdk.getSharedInstance().enablePushNotificationStorage()
        // we can set the attribution interval : https://docs.aiqua.appier.com/docs/event-attribution-ios-sdk
        //      the click-through attribution window (time interval) is set to 86,400 seconds (24 hours)
        QGSdk.getSharedInstance().setClickAttributionWindow(86400)
        //      the view-through attribution window is set to 3,600 seconds (1 hour).
        QGSdk.getSharedInstance().setAttributionWindow(3600)
        /* It is said the user can set the Event Attribution type in client side?
        //https://appier.atlassian.net/wiki/spaces/ETSKW/pages/2471920102/Create+your+first+app+using+Swift+and+SwiftUI#Event-Attribution-for-iOS
        // to set click through attribution window
        (void)setClickAttributionWindow:(NSInteger)CUSTOM_WINDOW;
        // to set view through attribution window
        (void)setAttributionWindow:(NSInteger)CUSTOM_WINDOW;
        // */
        
        // Registering Push Notification, or asking the user push notifiction consent
        if #available(iOS 10.0, *) {
            let center = UNUserNotificationCenter.current()
            center.delegate = self
            var options = UNAuthorizationOptions([.alert, .sound, .badge, .carPlay])
            
            // sample code to enable Provisional Authorization
            // https://appier.atlassian.net/wiki/spaces/ETSKW/pages/2471920102/Create+your+first+app+using+Swift+and+SwiftUI#Provisional-Push-Notifications-in-iOS-12
            if #available(iOS 12.0, *) {
              options.update(with: .provisional)
            }
            
            center.requestAuthorization(options: options) { (granted, error)in
              print("Granted: \(granted), Error: \(String(describing: error))")
            }
                        
        } else {
          // Fallback to earlier versions - iOS 8 & 9
          let settings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
          UIApplication.shared.registerUserNotificationSettings(settings)
        }

        // Request Tracking(IDFA collecting) Permission.
        if #available(iOS 14, *) {
            ATTrackingManager.requestTrackingAuthorization { _ in
                let qgsdk = QGSdk.getSharedInstance()
                //If true, IDFA is sent ONLY if user authorizes tracking
                //If false or not set, IDFA is sent regardless of user's authorization status
                qgsdk.setIDFAConsent(true)
            }
        }

        return true
    }
    
    // ASPN token. add these delegate methods to your AppDelegate class, so that the app will pass the push token to the Appier server.
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
      let QG = QGSdk.getSharedInstance()
        //print("My token is: \(deviceToken.description)")
        print("My token is: " + deviceToken.map { String(format: "%02x", $0) }.joined())
        QG.setToken(deviceToken as Data)
    }
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
      print("Failed to get token, Error: \(error.localizedDescription)")
    }

    // other push notification setting : https://docs.aiqua.appier.com/docs/handling-push-notifications-for-ios
    // 1. handling the click and deeplink events from push notification
    //    This method is also required for carousel and slider push to work.
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler:@escaping() -> Void) {
      QGSdk.getSharedInstance().userNotificationCenter(center, didReceive: response)
      print("push notification clicked")
      completionHandler()
    }
    // 2. Handling Background/Silent Push. pass completion handler UIBackgroundFetchResult accordingly
    //      not 100% arrives.  relavant to 'Wake app in background' checkbox on the campaign detail
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
      let QG = QGSdk.getSharedInstance()
      QG.application(application, didReceiveRemoteNotification: userInfo)
      print("Background push notification arrived")
      completionHandler(UIBackgroundFetchResult.noData)
    }
    // 3. Handling Foreground Push, to show notification even the app is foreground
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
      QGSdk.getSharedInstance().userNotificationCenter(center, willPresent: notification)
      print("Foreground push notification arrived")
      completionHandler([.alert, .badge, .sound]);
    }
    
    // we can receive key:value set at dashboard : https://docs.aiqua.appier.com/docs/customizing-push-notifications-ios
    func didReceive(_ request: UNNotificationRequest, withContentHandler contentHandler: @escaping (UNNotificationContent) -> Void) {
        let userinfo = request.content.userInfo
        if userinfo != nil {
            let customKeyPairs = userinfo["qgPayload"] as? [AnyHashable : Any]
            if let customKeyPair = customKeyPairs?["myKey"] {
                print("NotificationService EX: \(customKeyPair)")
            }
        }

//        // Existing QGNotificationSdk code ...
//        let qgsdk = QGNotificationSdk.sharedInstance(withAppGroup: "group.appier.dan-test-aq.notification")
//        qgsdk?.didReceive(request, withContentHandler: { content in
//            contentHandler(content)
//        })
    }

    // DeepLink or Custom URL ex) myapp://appier.dan-test-aq/product?prod_id=1
    //  https://developer.apple.com/documentation/xcode/defining-a-custom-url-scheme-for-your-app
    func application(_ application: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:] ) -> Bool {
        print("deeplink:" + url.absoluteString)
        // Determine who sent the URL.
        let sendingAppID = options[.sourceApplication]
        print("source application = \(sendingAppID ?? "Unknown")")

        // Process the URL.
        guard let components = NSURLComponents(url: url, resolvingAgainstBaseURL: true),
            let path = components.path,
            let params = components.queryItems else {
                print("Invalid URL or album path missing")
                return false
        }

        print(path, params)
        return true
    }
    func scene(_ scene: UIScene,willConnectTo session: UISceneSession,options connectionOptions: UIScene.ConnectionOptions) {

        // Determine who sent the URL.
        if let urlContext = connectionOptions.urlContexts.first {


            let sendingAppID = urlContext.options.sourceApplication
            let url = urlContext.url
            print("source application = \(sendingAppID ?? "Unknown")")
            print("url = \(url)")


            // Process the URL similarly to the UIApplicationDelegate example.
        }


        /*
         *
         */
    }

    
}


@main
struct dan_test_aqApp: App {
    
    // this line should be added after your creating of AppDelegate above
    @UIApplicationDelegateAdaptor(AppDelegate.self) var appDelegate
    
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
