//
//  ContentView.swift
//  dan-test-aq
//
//  Created by KR-Demo on 2021/10/15.
//

import SwiftUI


struct RoundedBlackButton: ViewModifier {
    func body(content: Content) -> some View {
        content
            .font(.body.bold())
            .buttonStyle(PlainButtonStyle())
            .foregroundColor(.black)
            .padding(8)
            .background(
                RoundedRectangle(cornerRadius: 8)
                    .fill(Color.black)
                    .shadow(color: .black, radius: 4, x: 0, y: 2)
            )
    }
}
struct RoundedWhiteButton: ViewModifier {
    func body(content: Content) -> some View {
        content
            .font(.body.bold())
            .buttonStyle(PlainButtonStyle())
            .padding(8)
            .background(
                RoundedRectangle(cornerRadius: 8)
                    .fill(Color.white)
                    .shadow(color: .black, radius: 4, x: 0, y: 2)
                )
    }
}

struct ContentView: View {
    @State private var showAlert: Bool = false
    @State private var showAlertEvent: Bool = false
    @State private var date = Date()
    @State private var isEmailValid : Bool   = true
    @State private var profile = UserProfile()
    @State private var event = EventParameter()
    
    let dateRange: ClosedRange<Date> = {
        let calendar = Calendar.current
        let date = Date()
        let components = calendar.dateComponents([.year, .month, .day], from: date)
        let year = components.year
        let startComponents = DateComponents(year: 1900, month: 1, day: 1)
        let endComponents = DateComponents(year: components.year, month: components.month, day: components.day)
        return calendar.date(from:startComponents)!
            ...
            calendar.date(from:endComponents)!
    }()
    
    var body: some View {
        NavigationView{
            VStack {
                VStack {
                    VStack {
                        VStack {
                            VStack {
                                Text("Daniel iOS App")
                                    .font(.title)
                                    .fontWeight(.heavy)
                                    .padding(4)
                                NavigationLink(destination: NewScreenView()) {
                                    Text("New Screen")
                                        .foregroundColor(Color.orange)
                                        .modifier(RoundedBlackButton())
                                }.navigationTitle("Daniel iOS App")
                                    .navigationBarHidden(true)
                                NavigationLink(destination: NewWebView(url: URL(string: "https://danjang-aiqua.herokuapp.com"))) {
                                    Text("My WebApp")
                                        .foregroundColor(Color.yellow)
                                        .modifier(RoundedBlackButton())
                                }.navigationTitle("Daniel iOS App")
                                    .navigationBarHidden(true)
                            }
                            .padding(8.0)
                            VStack {
                                Button(action: {
                                    self.showAlert = true
                                    profile.logging()
                                }) {
                                    /*@START_MENU_TOKEN@*/Text("Profile Logging")
                                        .foregroundColor(Color.red)
                                        .modifier(RoundedWhiteButton())
                                        .alert(isPresented: $showAlert){
                                            Alert(
                                                title: Text("Succeed"),
                                                message: Text("Profile logging sent"),
                                                dismissButton: .default(Text("OK"))
                                            )
                                        }
                                    
                                }
                                HStack {
                                    Text("user_id").frame(width: 120, alignment: Alignment.center);
                                    TextField("Placeholder", text: self.$profile.user_id).frame(width: 160, alignment: Alignment.leading)
                                    
                                }.padding(0)
                                HStack {
                                    Text("birthday").frame(width: 120, alignment: Alignment.center);
                                    VStack {
                                        DatePicker("",
                                                   selection: $profile.birthdayDate,
                                                   in: dateRange,
                                                   displayedComponents: [.date]).frame(width:160, alignment: Alignment.leading)
                                            .datePickerStyle(CompactDatePickerStyle())
                                            .clipped()
                                            .labelsHidden()
                                        
                                    }
                                    
                                }.padding(0)
                                HStack {
                                    Text("email").frame(width: 120, alignment: Alignment.center);
                                    TextField("Placeholder", text:self.$profile.email)
                                        .frame(width: 160, alignment: Alignment.leading)
                                        .autocapitalization(.none)
                                    
                                }.padding(0)
                            }.padding(0)
                            VStack {
                                Button(action: {
                                    self.showAlertEvent = true
                                    event.logging()
                                }) {
                                    Text("Event Logging")       .foregroundColor(Color.blue) .modifier(RoundedWhiteButton())
                                        .alert(isPresented: $showAlertEvent) {
                                            Alert(
                                                title: Text("Succeed"),
                                                message: Text("Event Logging sent"),
                                                dismissButton: .default(Text("OK"))
                                            )
                                        }
                                    
                                }
                                HStack {
                                    Text("eventName")
                                        .frame(width: 120, alignment: Alignment.center); TextField("product_viewed", text: $event.eventName).frame(width: 160, alignment: Alignment.leading)
                                    
                                }.padding(0)
                                HStack {
                                    Text("product_id")
                                        .frame(width: 120, alignment: Alignment.center);
                                    TextField("1111", value: $event.product_id,formatter: NumberFormatter()).frame(width: 160, alignment: Alignment.leading)
                                    
                                }.padding(0)
                                HStack {
                                    Text("price").frame(width: 120, alignment: Alignment.center);
                                    TextField("20.5", value:$event.price,formatter: NumberFormatter())
                                        .frame(width: 160, alignment: Alignment.leading)
                                    
                                }.padding(0)
                                
                            }.padding(0)
                        }
                    }
                }
            }
            .background(Color(UIColor.systemBackground))
            // for dark mode
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            ContentView()
        }
    }
}
