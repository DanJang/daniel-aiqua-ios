//
//  NewWebView.swift
//  dan-test-aq
//
//  Created by KR-Demo on 2021/10/31.
//

import Foundation
import SwiftUI
import WebKit
import UIKit
import Appier  // for QGWKWebView


struct NewWebView: UIViewRepresentable {
    // this value will be set at ContentView.swift
    let url: URL?
    
    /* 1. no bridge
    // * This webview is stand alone, not related to native part of this app. userId of the two space differ
    // * profile&event on the webview go to d023d23bac0cf1e8e545_web, p&e on native app go to _ios
    // * Unlike my expectation, the userId persists even though I restart the iPhone11 in simulator
    func makeUIView(context: Context) -> WKWebView {
        //Creates the view object and configures its initial state.
        let prefs = WKWebpagePreferences()
        prefs.allowsContentJavaScript = true
        let config = WKWebViewConfiguration()
        config.defaultWebpagePreferences = prefs
        return WKWebView(frame: .zero, configuration: config)
    }
     // */

    
    ///* 2. bridge by QGWKWebView
    // * copied from 'integrate webview support' part in  https://appier.atlassian.net/wiki/spaces/ETSKW/pages/2471920102/Create+your+first+app+using+Swift+and+SwiftUI
    // * Now webpage uses userId of native part of this app represents the whole
    // * profile&event occurring on the webview all go to d023d23bac0cf1e8e545_dev_ios
    let SCRIPT: String = "var native={log:function(s){try{window.webkit.messageHandlers.log.postMessage(s)}catch(e){}}};"
    func makeUIView(context: Context) -> WKWebView {
        let prefs = WKWebpagePreferences()
        prefs.allowsContentJavaScript = true
        let config = WKWebViewConfiguration()
        config.defaultWebpagePreferences = prefs
        
        let webview = QGWKWebView(frame: .zero, configuration: config)
        let userScript = WKUserScript.init(source: SCRIPT, injectionTime: .atDocumentStart, forMainFrameOnly: true)
        webview.inject(userScript)
        webview.addScriptMessageHandler("log")
        return webview
    }
    // */
    
    func updateUIView(_ uiView: WKWebView, context: Context) {
        guard let myURL = url else {
            return
        }
        let request = URLRequest(url: myURL)
        
        uiView.load(request)
        
    }
    
    

}

// this lines should exists?
struct NewWebView_Previews: PreviewProvider {
    static var previews: some View {
        NewWebView(url: URL(string:"https://danjang-aiqua.herokuapp.com"))
    }
}



// 3-1. bridge by ViewController. Preferred than 3-2 because QG features will automatically be updated
// https://docs.aiqua.appier.com/docs/ios-webview-support
class QGWebViewController: UIViewController, QGWKScriptMessageHandler {

    var webview: QGWKWebView = QGWKWebView()
    let SCRIPT: String = "var native={log:function(s){try{window.webkit.messageHandlers.log.postMessage(s)}catch(e){}}};"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.webview = QGWKWebView.init(frame: self.view.frame)
        self.webview.delegate = self
        
        let userScript = WKUserScript.init(source: SCRIPT, injectionTime: .atDocumentStart, forMainFrameOnly: true)
        self.webview.inject(userScript)
        self.webview.addScriptMessageHandler("log")
        self.view.addSubview(self.webview)
        self.webview.load(URLRequest.init(url: URL(string: "https://danjang-aiqua.herokuapp.com")!))
        
    }
    
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        if message.name == "log" {
            print("log from javascript : \(message.body)")
        }
    }
}

// 3-2. bridge by customization
// https://docs.aiqua.appier.com/docs/custom-implementation-of-wkwebview
class WebViewController: UIViewController, WKScriptMessageHandler {
    
    @IBOutlet var webView:WKWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
                
        // define a global static string for script
        let SCRIPT: String = "var aiqMobileSdk={promises:{},logEvent:function(e){try{var r=JSON.parse(e);r.command='event',window.webkit&&window.webkit.messageHandlers&&window.webkit.messageHandlers.aiqua.postMessage(r)}catch(e){}},setCustomKey:function(e){try{var r=JSON.parse(e);r.command='profile',window.webkit&&window.webkit.messageHandlers&&window.webkit.messageHandlers.aiqua.postMessage(r)}catch(e){}},getRecommendationByScenario:function(e){var r=this,t=JSON.parse(e);return new Promise(function(e,s){var a=r.generateUUID();r.promises[a]={resolve:e,reject:s};var n={command:command='getRecommendation',promiseId:a,scenarioId:t.scenarioId,productId:t.productId,query:{filter:t.filter,num:t.num,user_id:t.user_id}};try{window.webkit.messageHandlers.aiqua.postMessage(n)}catch(e){}})},generateUUID:function(){var e=(new Date).getTime();return'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g,function(r){var t=(e+16*Math.random())%16|0;return e=Math.floor(e/16),('x'==r?t:3&t|8).toString(16)})},resolvePromise:function(e,r,t){t?this.promises[e].reject(r):this.promises[e].resolve(r),delete this.promises[e]}}"
        
        // use the above script to create WKUserScript
        let userScript = WKUserScript.init(source: SCRIPT, injectionTime: .atDocumentStart, forMainFrameOnly: true)

        // add the user script to the webview user controller
        webView.configuration.userContentController.addUserScript(userScript)
        webView.configuration.userContentController.add(self, name:"aiqua")
        webView.load(URLRequest(url: URL(string: "https://danjang-aiqua.herokuapp.com")!))
    }
    
    func userContentController(_ userContentController: WKUserContentController,
                                    didReceive message: WKScriptMessage) {
        if message.name == "aiqua" {
            guard let body = message.body as? [String: Any] else { return }
            guard let command = body["command"] as? String else { return }
            let QG = QGSdk.getSharedInstance()
            if command == "event" {
                QG.logEvent((body["eventName"] as! String), withParameters: (body["parameters"] as? [String: Any]), withValueToSum: (body["vts"] as? NSNumber))
            } else if command == "profile" {
                QG.setCustomKey((body["key"] as! String), withValue: body["value"])
            } else if command == "getRecommendation" {

                let promiseId:String = (body["promiseId"] as? String) ?? ""
                let scenarioId:String = (body["scenarioId"] as? String) ?? ""
                let productId:String = (body["productId"] as? String) ?? ""
                QG.getRecommendationWithScenarioId(scenarioId, withProductId:productId, withQueryParameters:body["query"] as? [String:Any]) { (response) in
                    DispatchQueue.main.async {
                        var jsonResult = "{}"
                        var hasError = true;
                        if (response != nil) {
                            do {
                                let jsonData:Data? = try JSONSerialization.data(withJSONObject: response as! [String:Any], options: [])
                                if (jsonData == nil) {
                                    jsonResult = "JSON parse error"
                                } else {
                                    jsonResult = String(data: jsonData!, encoding: .utf8)!
                                    hasError = false
                                }
                            } catch let parsingError {
                                print("Error", parsingError)
                            }
                        }
                        self.qgSendMessage(toJavaScript: promiseId, data: jsonResult, error: hasError)
                    }
                }
            }
        } else {
            // handle other messages
        }
    }
    
    func qgSendMessage(toJavaScript promiseId: String?, data: String?, error: Bool) {
        let execString = "aiqMobileSdk.resolvePromise(`\(promiseId ?? "")`, `\(data ?? "")`, \(error))"
        webView.evaluateJavaScript(execString, completionHandler: { result, error in
            if let error = error {
                print("callback to js generated error -- \(error)")
            }
        })
    }
}

