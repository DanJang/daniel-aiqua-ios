//
//  NewScreen.swift
//  dan-test-aq
//
//  Created by KR-Demo on 2021/10/31.
//

import Foundation
import SwiftUI
//import Appier



struct NewScreenView: View {
    @State private var event = EventParameter()
    var body: some View {
            Text("New Screen")
                .font(.title)
                .fontWeight(.bold)
                .navigationTitle("New Screen Title")
                .navigationBarTitleDisplayMode(.inline)
                .padding(10)
//            Text("My REC")
//                .font(.title)
//                .fontWeight(.bold)
//                .navigationTitle("My REC")
//                .navigationBarTitleDisplayMode(.inline)
        
    }
    
}

struct NewScreenView_Previews: PreviewProvider {
    static var previews: some View {
        NewScreenView()
    }
}

